package example;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.net.URISyntaxException;

import dsaas.Auth;
import dsaas.VersionControl;
import dsaas.CloudStructureFactory;
import dsaas.GraphCloudStructure;
import dsaas.MapCloudStructure;

import java.util.Scanner;
import java.io.File;


class PaymentObj {
	String payee, fundingSourceDescription, city;
	double amount;

	public PaymentObj(String payee, String fundingSourceDescription, String city, double amount) {
		this.payee = payee;
		this.fundingSourceDescription = fundingSourceDescription;
		this.city = city;
		this.amount = amount;
	}
}

public class InsertionExperiment {
	public static void main(String [] args) {
		experiment("exp1", 0, 500);
		experiment("exp1", 500, 1000);
		// experiment("exp2", 2000);
		// experiment("exp3", 4000);
		// experiment("exp4", 8000);
		// experiment("exp5", 16000);
		// experiment("exp6", 32000);
	}

	public static void experiment(String id, int start, int end) {
		String namespace = "pierre";

		VersionControl<MapCloudStructure> mapVersionControl = new CloudStructureFactory<MapCloudStructure>(namespace, id, MapCloudStructure.class, PaymentObj.class).getVersionControl();
		MapCloudStructure<PaymentObj> cloudmap = mapVersionControl.initialVersion();

		long startTime = System.currentTimeMillis();
		long endTime = 0;

		try {
			Scanner sc = new Scanner(new File("./checkbook 2014.csv"));
			int count = 0;
			sc.nextLine();
			while (sc.hasNext()) {
				String line = sc.nextLine();
				count++;
				try {
					if (start <= count && count < end) {
						String [] data = line.split(",");
						cloudmap.put(data[0], new PaymentObj(data[1], data[11], data[2], Double.parseDouble(data[9])));
					}
					if (count == end) {
						endTime = System.currentTimeMillis();
						System.out.println(endTime - startTime);
						System.out.println(count);
					}
					count++;
				} catch (Exception e) {}
			}
			System.out.println("Lines: " + count);
		} catch (Exception e) {
			e.printStackTrace();
		}

		System.out.println("Time: " + (endTime - startTime));
	}
}