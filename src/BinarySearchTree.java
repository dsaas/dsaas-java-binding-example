package example;

import dsaas.Auth;
import dsaas.VersionControl;
import dsaas.CloudStructureFactory;
import dsaas.GraphCloudStructure;

import java.util.PriorityQueue;
import java.util.Collection;
import java.lang.Comparable;

class Node {

	public Integer key;
	public String value;

	public Node(int key, String value) {
		this.key = key;
		this.value = value;
	}
}

class Link {

	public String nodeName;
	public String linkName;

	public Link(String linkName, String nodeName) {
		this.nodeName = nodeName;
		this.linkName = linkName;
	}
}


public class BinarySearchTree {

	private CloudStructureFactory<GraphCloudStructure> myGraphFactory;
	private GraphCloudStructure<Node, Link> graph;

	public BinarySearchTree(String namespace, String graphName) {
		this.myGraphFactory = new CloudStructureFactory<GraphCloudStructure>(namespace, graphName, GraphCloudStructure.class, Node.class, Link.class);
	}

	public VersionControl<GraphCloudStructure> getVersionControl() {
		return this.myGraphFactory.getVersionControl();
	}

	public void setGraph(GraphCloudStructure<Node, Link> graph) {
		this.graph = graph;
	}

	public void put(int key, String value) {
		put("root", key, value);
	}

	private boolean put(String nodeName, Integer key, String value) {

		Node node = this.graph.getVertex(nodeName);

		if (node == null) {
			this.graph.addVertex(nodeName, new Node(key, value));
			return true;
		} else {
			String linkName = "";
			if (node.key.compareTo(key) < 0) {
				linkName = nodeName + "-left";
			} else if (node.key.compareTo(key) > 0) {
				linkName = nodeName + "-right";
			} else {
				this.graph.updateVertex(nodeName, new Node(key, value));
				return false;
			}

			Link link = findLink(this.graph.getEdgesOfVertex(nodeName), linkName);
			if (link == null) {
				link = new Link(linkName, key+"");
			}

			if (put(link.nodeName, key, value)) {
				this.graph.addEdge(nodeName, link.nodeName, link);
			}
			return false;
		}
	}

	private Link findLink(Collection<Link> links, String linkName) {
		for (Link link : links)
			if (link.linkName.equals(linkName))
				return link;
		return null;
	}

	public String get(int key) {
		return get("root", key);
	}

	private String get(String nodeName, Integer key) {
		Node node = this.graph.getVertex(nodeName);
		if (node == null) {
			return null;
		} else {
			String linkName = "";
			if (node.key.compareTo(key) < 0) {
				linkName = nodeName + "-left";
			} else if (node.key.compareTo(key) > 0) {
				linkName = nodeName + "-right";
			} else {
				return node.value;
			}

			Link link = findLink(this.graph.getEdgesOfVertex(nodeName), linkName);
			return get(link.nodeName, key);
		}
	}

	public static void main(String [] args) {

		String namespace = "pierre";
		String graphName = "BinarySearchTree";

		BinarySearchTree bst = new BinarySearchTree(namespace, graphName);
		bst.setGraph(bst.getVersionControl().initialVersion());

		bst.put(10, "koos");
		bst.put(5, "ashley");
		bst.put(3, "kobie");
		bst.put(6, "frank");
		bst.put(4, "peter");
		bst.put(11, "renaldo");

		System.out.println(bst.get(6));
	}
}