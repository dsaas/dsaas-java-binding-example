package example;

import dsaas.Auth;
import dsaas.VersionControl;
import dsaas.CloudStructureFactory;
import dsaas.GraphCloudStructure;

import java.util.PriorityQueue;
import java.util.Collection;
import java.lang.Comparable;

class Town implements Comparable {
	String name;
	String fromTown;
	boolean visited = false;
	int distanceTravelled;

	public Town(String name) {
		this.name = name;
	}

	@Override
	public int compareTo(Object o) {
		Town other = (Town) o;
		return this.distanceTravelled - other.distanceTravelled;
	}
}

class Road {
	int distance;

	public Road(int distance) {
		this.distance = distance;
	}
}


public class Dijkstra {
	public static void main(String [] args) {

		String namespace = "pierre";
		String graphName = "example-graph";

		CloudStructureFactory<GraphCloudStructure> myGraphFactory = new CloudStructureFactory<GraphCloudStructure>(namespace, graphName, GraphCloudStructure.class, Town.class, Road.class);
		VersionControl<GraphCloudStructure> graphVersionControl = myGraphFactory.getVersionControl();
		GraphCloudStructure<Town, Road> graph = graphVersionControl.initialVersion();

		// Build the graph
		// String builtGraphVersion = graphVersionControl.getVersionFromTag("built-graph");
		// if (builtGraphVersion == null) {
			String [] townNames = {"S", "A", "B", "C", 	"D", "E", "F", "T"};
			for (String name : townNames) {
				graph.addVertex(name, new Town(name));
			}
			
			String [] startTown = {"S", "S", "S", "A", "A", "A", "B", "B", "B", "C", "F", "D", "E"};
			String [] endTown   = {"A", "B", "C", "B", "D", "F", "D", "E", "C", "E", "T", "T", "T"};
			int [] distance     = { 2,   5,   4 ,  2,   7,   12,  4,   3 ,  1 ,  4 ,  3 ,  5 ,  7 };

			for (int i = 0; i < startTown.length; i++) {
				graph.addEdge(startTown[i], endTown[i], new Road(distance[i]));
			}
			// graphVersionControl.addTag("built-graph", graph.getVersion());
		// } else {
		// 	graph = graphVersionControl.get(builtGraphVersion);
		// }

		// Do Dijkstra
		// PriorityQueue<Town> pq = new PriorityQueue<Town>();
		// pq.add(graph.getVertex("S"));

		// while (!pq.isEmpty()) {
		// 	Town town = pq.poll();
		// 	if (town.name == "T") break;
		// 	town.visited = true;
		// 	// graph.updateVertex(town.name, town);
		// 	Collection<Town> neighbourTowns = graph.adjacent(town.name);
		// 	for (Town ntown : neighbourTowns) {
		// 		Road road = graph.getEdge(town.name, ntown.name);
		// 		if (road == null) road = graph.getEdge(ntown.name, town.name);
		// 		ntown.distanceTravelled = town.distanceTravelled + road.distance;
		// 		ntown.fromTown = town.name;

		// 		graph.updateVertex(ntown.name, ntown);

		// 		pq.add(ntown);
		// 	}
		// }
	}
}